<?php get_header(); ?>
<div id="titler" class="wrap-f">
		<div class="wrap-s clr">
			<h2>Knowledge Center</h2>

			<div class="breadcrumbs-box">
				<a href="#" class="breadcrumbs-line"><span>Home</span></a>
				<a href="#" class="breadcrumbs-line">Knowledge Center</a>
			</div>
		</div>
	</div>

	<div id="main" class="wrap-f">
		<div class="wrap-s clr">
			<div class="main-lg-box">
				<div class="page-box clr">
					<h3 class="title">Knowledge Center</h3>
					<div class="last-news-box clr">

						<div class="last-news-line clr">
							<a href="#"><img src="<?php bloginfo('template_directory'); ?>/files/home_photo_4.jpg" alt=""></a>
							<h4><a href="#">Answers to your security questions</a></h4>
							<p>
								Security is Securitas' core business and we can provide our customers with a range of specialized services or a complete security solution. Here you will find information on our most frequent services.
							</p>
						</div>

						<div class="last-news-line clr">
							<a href="#"><img src="<?php bloginfo('template_directory'); ?>/files/home_photo_4.jpg" alt=""></a>
							<h4><a href="#">Knowledge Center Resources</a></h4>
							<div class="resurses">
								<a href="#" class="read-more pseudo-link">Reference Guides</a><br>
								<a href="#" class="read-more pseudo-link">Industry News</a><br>
								<a href="#" class="read-more pseudo-link">Safety Awareness Tips</a><br>
								<a href="#" class="read-more pseudo-link">News</a><br>
							</div>
						</div>

						<div class="last-news-line clr">
							<a href="#"><img src="<?php bloginfo('template_directory'); ?>/files/home_photo_4.jpg" alt=""></a>
							<h4><a href="#">Whitepapers</a></h4>
							<p>
								CPS USA wants to share security knowledge and experience. Read our whitepapers to stay informed about evolving security trends.
							</p>
							<div class="resurses">
								<a href="#" class="read-more pseudo-link">Workspace Violence: Prevention</a><br>
								<a href="#" class="read-more pseudo-link">ACA Impact on Healthcare Facillities</a><br>
								<a href="#" class="read-more pseudo-link">Security Alarm Response: The Mobile Solution</a><br>
							</div>
						</div>

						<div class="last-news-line clr">
							<a href="#"><img src="<?php bloginfo('template_directory'); ?>/files/home_photo_4.jpg" alt=""></a>
							<h4><a href="#">Industry News</a></h4>
							<p>
								We are commitled to helping protect our customers assets and people, so it is important to remain current on industry trends. CPS USA is featured in some industry newsletters to discuss our perspective on security trends and best practices.
							</p>
							<div class="resurses">
								<a href="#" class="read-more pseudo-link">ASIS Foundation - Workforce Compentencies</a>
							</div>
						</div>

					</div>

					<div class="clr"></div>
					
					

				</div>
			</div>
						<div class="main-sm-box">
		<h3 class="title">Sections</h3>
			<button class="accordion">Securitas Group</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>

<button class="accordion">Our Values</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>

<button class="accordion">Our Approach</button>
<div class="panel">
  <a href="#" class="line">Security Services North America</a>
  <a href="#" class="line">Security Services Europe</a>
  <a href="#" class="line">Security Services Ibero-America</a>
</div>
<button class="accordion">Our Partnerships</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>

<button class="accordion">Our Organization</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Our History</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Safety Act</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Our Responsibility</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Supplier Diversity Program</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Contact Us</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
</div>

		</div>
</div>

	<?php get_footer(); ?>