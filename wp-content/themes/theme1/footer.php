	
	<div id="footer" class="wrap-f clr">
		<div class="wrap-s clr">
			<div class="footer-box">
				<div class="footer-line">
					<a href="#" class="logo"></a>
					<p class="about">
					Collective Protective Services provides armed and unarmed security guards to California businesses and organizations.
					</p>
					<div class="subscribe-box">
						<h3 class="title">Subscribe</h3>
						<div class="search-form">
										<?php echo do_shortcode('[mc4wp_form id="366"]'); ?>
								<!-- <button type="submit" class="btn-blue">Ок</button> -->
						
						</div>
					</div>
				</div>
				<div class="footer-line">
					<h3 class="title">Lastest News</h3>
					<div class="new-box">  
					<ul>
 <?php $the_query = new WP_Query( 'showposts=2' ); ?>
<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
<li class="new-box"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
<li class="new-box"><?php echo substr(strip_tags($post->post_content), 0, 250);?>
<hr>
</li> <?php endwhile;?>
</ul>
					</div>
				</div>
				<div class="clr footer-clr"></div>
				<div class="footer-line">
				<h3 class="title">Twitter Feed</h3>
					<div class="twitter-box">
						<?php if ( is_active_sidebar( 'home_right_1' ) ) : ?>

						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							
							<?php dynamic_sidebar( 'home_right_1' ); ?>
						<hr>
						</div><!-- #primary-sidebar -->
					<?php endif; ?>
					</div>
					</div>
				</div>
			<div class="footer-line">
					<h3 class="title">Photo Stream</h3>
					<div class="photo-box clr">
						<?php echo do_shortcode('[jr_instagram id="2"]'); ?>
					</div>
				</div>
				<div class="clr footer-clr h30"></div>
			</div>
		</div>
	</div>

	<div id="bottomer" class="wrap-f clr">
		<div class="wrap-s clr">
			<div class="pull-left">Copyright &copy; 2014 cpssecurity.com - Collective Protective Services, USA</div>
			<div class="pull-right">
				<div class="social-box">
					<div class="social-title"><a href="#" class="pseudo-link">Site Map</a></div>
					<div class="social-title"><a href="#" class="pseudo-link">Declaimer</a></div>
	  				<a href="http://fb.com/" class="social-line"><i class="kicon soc-fb"></i></a>
	  				<a href="http://twitter.com/" class="social-line"><i class="kicon soc-tw"></i></a>
	  				<a href="http://vk.com/" class="social-line"><i class="kicon soc-gp"></i></a>
	  				<a href="http://instagram.com" class="social-line"><i class="kicon soc-in"></i></a>
	  				<a href="http://www.odnoklassniki.ru/" class="social-line"><i class="kicon soc-vm"></i></a>
	  			</div>
			</div>
		</div>
	</div>

<?php wp_footer(); ?>
</body>
</html>