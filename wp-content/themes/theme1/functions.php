<?php
//thumbnails
add_theme_support( 'post-thumbnails' );
//headermenu
add_theme_support('widgets');

function theme_register_nav_menu() {
	register_nav_menus(
   array('primary'=> 'Главное меню','menu1' => 'Меню верх')
);
}
add_action( 'after_setup_theme', 'theme_register_nav_menu' );

function theme_name_scripts() {
	wp_enqueue_style( 'main-style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );


function stylees() {
    wp_enqueue_style( 'styles', get_stylesheet_directory_uri() . '/sidebar-menu.css',"6.0");
}
add_action( 'wp_enqueue_scripts', 'stylees' );

function wpdocs_owlstyles_method() {
    wp_enqueue_style( 'owlstyle', get_stylesheet_directory_uri() . '/owlcarousel/assets/owl.carousel.css',"6.0");
}
add_action( 'wp_enqueue_scripts', 'wpdocs_owlstyles_method' );


function wpdocs_owlstyles_method1() {
    wp_enqueue_style( 'owlstyle1', get_stylesheet_directory_uri() . '/owlcarousel/assets/owl.theme.default.min.css',"6.0");
}
add_action( 'wp_enqueue_scripts', 'wpdocs_owlstyles_method1' );

function wpdocs_owl() {
    wp_enqueue_script( 'owlscr', get_stylesheet_directory_uri() . '/owlcarousel/owl.carousel.js', array( 'jquery' ) ,"6.0");
}
add_action( 'wp_enqueue_scripts', 'wpdocs_owl' );


function wpdocs_scripts_method() {
    wp_enqueue_script( 'mainscrt', get_stylesheet_directory_uri() . '/js/main.js', array( 'jquery' ) );
}
add_action( 'wp_enqueue_scripts', 'wpdocs_scripts_method' );

function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Home right sidebar',
		'id'            => 'home_right_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );
add_action( 'wp_enqueue_scripts', 'my_scripts_method' );
function my_scripts_method(){
	wp_enqueue_script( 'newscript', get_template_directory_uri() . '/js/sidebar-menu.js');
}

add_action( 'wp_enqueue_scripts', 'method' );
function method(){
	wp_enqueue_script( 'newscript', get_template_directory_uri() . '/js/script.js');
}
function wpb_list_child_pages() { 
global $post; 
if ( is_page() && $post->post_parent )
	$childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
else
	$childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );
if ( $childpages ) {
	$string = '<ul>' . $childpages . '</ul>';
}
return $string;
}
add_shortcode('wpb_childpages', 'wpb_list_child_pages');
?>