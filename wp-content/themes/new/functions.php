<?php
//thumbnails
add_theme_support( 'post-thumbnails' );
//headermenu


function theme_name_scripts() {
	wp_enqueue_style( 'style-name', get_stylesheet_uri() );
}
	add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );
?>
<?php
/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Home right sidebar',
		'id'            => 'home_right_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );


?>
<?php



function wpdocs_owlstyles_method() {
    wp_enqueue_style( 'owlstyle', get_stylesheet_directory_uri() . '/owlcarousel/assets/owl.carousel.css',"7.0");
}
add_action( 'wp_enqueue_scripts', 'wpdocs_owlstyles_method' );


function wpdocs_owlstyles_method1() {
    wp_enqueue_style( 'owlstyle1', get_stylesheet_directory_uri() . '/owlcarousel/assets/owl.theme.default.min.css',"7.0");
}
add_action( 'wp_enqueue_scripts', 'wpdocs_owlstyles_method1' );

function wpdocs_owl() {
    wp_enqueue_script( 'owlscr', get_stylesheet_directory_uri() . '/owlcarousel/owl.carousel.js', array( 'jquery' ) ,"7.0");
}
add_action( 'wp_enqueue_scripts', 'wpdocs_owl' );


function wpdocs_scripts_method() {
    wp_enqueue_script( 'mainscrt', get_stylesheet_directory_uri() . '/js/main.js', array( 'jquery' ) ,"7.0");
}
add_action( 'wp_enqueue_scripts', 'wpdocs_scripts_method' );




?>
