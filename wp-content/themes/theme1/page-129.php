<?php get_header(); ?>
<div id="titler" class="wrap-f">
		<div class="wrap-s clr">
			<h2>About Us</h2>

			<div class="breadcrumbs-box">
				<a href="http://wp.dev/" class="breadcrumbs-line"><span>Home</span></a>
				<a href="#" class="breadcrumbs-line">About Us</a>
			</div>
		</div>
	</div>

	<div id="main" class="wrap-f">
		<div class="wrap-s clr">
			<div class="main-lg-box">
				<div class="page-box clr">
					<h3 class="title">News</h3>
					<div class="text-box">
						<img src="<?php bloginfo('template_directory'); ?>/files/slide_1.jpg" alt="" class="full-image">
						<h4>Commercial Building Security</h4>
						<p>
							Securitas is a global knowledge leader in security. From a broad range of services of specialized guarding, technology solutions and consulting and investigations, we customize offerings that are suited to the individual customer’s needs, in order to deliver the most effective security solutions. Everywhere from small stores to airports, our 310,000 employees are making a difference.
							<br><br>
							The glue that holds together Securitas’ decentralized organization is a strong corporate culture. It is a culture distinguished by responsibility, ownership, entrepreneurship and what we call the Securitas Model.
						</p>
					</div> 


<?php if ( have_posts() ):  while ( have_posts() ) :the_post();?>
  <div class="last-news-box clr">

						<div class="last-news-line clr">
					
							
                                <a><?php the_post_thumbnail(); ?></a>
							
						     <div class="date"><?php the_date('d.m.y');?></div>
                 <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                 <?php the_content () ?>
							
						</div>
	</div>                      

	<div class="clr"></div>
             
     <?php the_tags(); ?>
                       
                 
    
 <?php endwhile; ?>

<?php else :?>

<?php endif; ?>


				</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>