	<div id="footer" class="wrap-f clr">
		<div class="wrap-s clr">
			<div class="footer-box">
				<div class="footer-line">
					<a href="#" class="logo"></a>
					<p class="about">
					Collective Protective Services provides armed and unarmed security guards to California businesses and organizations.
					</p>
					<div class="subscribe-box">
						<h3 class="title">Subscribe</h3>
						<div class="search-form">
							<form action="#" method="GET">
								<input type="text" value="" name="query" placeholder="Sign up for weekly email updates">
								<button type="submit" class="btn-blue">Ок</button>
							</form>
						</div>
					</div>
				</div>
				<div class="footer-line">
					<h3 class="title">Lastest News</h3>
					<div class="new-box">  
						<a href="#">The easiest way to promote</a>
						<p>Easily view and keep track of all links you've created in the past. Access to edit, view stats, and copy your links are all easily accessible.</p>
						<hr>
						<a href="#">Advertise your stuff on Twitter</a>
						<p>Easily view and keep track of all links you've created in the past. Access to edit, view stats, and copy your links are all easily accessible.</p>
						<hr>
					</div>
				</div>
				<div class="clr footer-clr"></div>
				<div class="footer-line">
					<h3 class="title">Twitter Feed</h3>
					<div class="twitter-box">
						<script src="http://widgets.twimg.com/j/2/widget.js"></script>
<script>
new TWTR.Widget({
  version: 2,
  type: 'search',
  search: '#asus',
  interval: 6000,
  title: 'Твиты по тегу',
  subject: 'Asus',
  width: 250,
  height: 300,
  theme: {
    shell: {
      background: '#8ec1da',
      color: '#ffffff'
    },
    tweets: {
      background: '#ffffff',
      color: '#444444',
      links: '#1985b5'
    }
  },
  features: {
    scrollbar: false,
    loop: true,
    live: true,
    hashtags: true,
    timestamp: true,
    avatars: true,
    toptweets: true,
    behavior: 'default'
  }
}).render().start();
</script>
					</div>
				</div>
				<div class="footer-line">
					<h3 class="title">Photo Stream</h3>
					<div class="photo-box clr">
						<div class="photo-line"><img src="<?php bloginfo('template_directory'); ?>/files/footer_photo_1.jpg">
						</div>
						<div class="photo-line"><img src="<?php bloginfo('template_directory'); ?>/files/footer_photo_2.jpg">
						</div>
						<div class="photo-line"><img src="<?php bloginfo('template_directory'); ?>/files/footer_photo_3.jpg">
						</div>
						<div class="photo-line"><img src="<?php bloginfo('template_directory'); ?>/files/footer_photo_4.jpg">
						</div>
						<div class="photo-line"><img src="<?php bloginfo('template_directory'); ?>/files/footer_photo_5.jpg">
						</div>
						<div class="photo-line"><img src="<?php bloginfo('template_directory'); ?>/files/footer_photo_3.jpg">
						</div>
						<div class="photo-line"><img src="<?php bloginfo('template_directory'); ?>/files/footer_photo_6.jpg">
						</div>
						<div class="photo-line"><img src="<?php bloginfo('template_directory'); ?>/files/footer_photo_1.jpg">
						</div>
					</div>
				</div>
				<div class="clr footer-clr h30"></div>
			</div>
		</div>
	</div>

	<div id="bottomer" class="wrap-f clr">
		<div class="wrap-s clr">
			<div class="pull-left">Copyright &copy; 2014 cpssecurity.com - Collective Protective Services, USA</div>
			<div class="pull-right">
				<div class="social-box">
					<div class="social-title"><a href="#" class="pseudo-link">Site Map</a></div>
					<div class="social-title"><a href="#" class="pseudo-link">Declaimer</a></div>
	  				<a href="http://fb.com/" class="social-line"><i class="kicon soc-fb"></i></a>
	  				<a href="http://twitter.com/" class="social-line"><i class="kicon soc-tw"></i></a>
	  				<a href="http://vk.com/" class="social-line"><i class="kicon soc-gp"></i></a>
	  				<a href="http://instagram.com" class="social-line"><i class="kicon soc-in"></i></a>
	  				<a href="http://www.odnoklassniki.ru/" class="social-line"><i class="kicon soc-vm"></i></a>
	  			</div>
			</div>
		</div>
	</div>



</body>
</html>