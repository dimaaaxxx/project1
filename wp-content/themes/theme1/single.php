<?php get_header(); ?>
<div id="titler" class="wrap-f">
		<div class="wrap-s clr">
			<h2>About Us</h2>

			<div class="breadcrumbs-box">
				<a href="#" class="breadcrumbs-line"><span>Home</span></a>
				<a href="#" class="breadcrumbs-line">News</a>
				<a href="#" class="breadcrumbs-line"><?php  the_title()?></a>
			</div>
		</div>
	</div>

	<div id="main" class="wrap-f">
		<div class="wrap-s clr">
			<div class="main-lg-box">
				<div class="page-box no-overflow">
					<h3 class="title"><?php the_title(); ?></h3>
					<div class="text-box">
					<?php $thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(0), 'full' ); ?>
						<img src="<?php echo $thumbnail_attributes[0] ?>" alt="" class="full-image">
					<?php if ( have_posts() ):  while ( have_posts() ) :the_post();?>

                 <?php the_content () ?>
                 <div class="date"><?php the_time('d / m / y');?></div>		   
 <?php endwhile; ?>
<?php else :?>
<?php endif; ?>	
					</div>
					
				</div>
			</div>
			<div class="main-sm-box">
		<h3 class="title">Sections</h3>
			<button class="accordion">Securitas Group</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>

<button class="accordion">Our Values</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>

<button class="accordion">Our Approach</button>
<div class="panel">
  <a href="#" class="line">Security Services North America</a>
  <a href="#" class="line">Security Services Europe</a>
  <a href="#" class="line">Security Services Ibero-America</a>
</div>
<button class="accordion">Our Partnerships</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>

<button class="accordion">Our Organization</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Our History</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Safety Act</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Our Responsibility</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Supplier Diversity Program</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Contact Us</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
</div>

		</div>
</div>
	<?php get_footer(); ?>