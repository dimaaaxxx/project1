<?php get_header(); ?>
<div id="titler" class="wrap-f">
		<div class="wrap-s clr">
			<h2>MMJ</h2>

			<div class="breadcrumbs-box">
				<a href="#" class="breadcrumbs-line"><span>Home</span></a>
				<a href="#" class="breadcrumbs-line">MMJ</a>
			</div>
		</div>
	</div>

	<div id="main" class="wrap-f">
		<div class="wrap-s clr">
			<div class="main-lg-box">
				<div class="page-box no-overflow">
					<h3 class="title">MMJ</h3>
					<div class="text-box">
						<img src="<?php bloginfo('template_directory'); ?>/files/slide_1_long.png" alt="" class="full-image">
						
						<p>
						Securitas is a global knowledge leader in security. From a broad range of services of specialized guarding, technology solutions and consulting and investigations, we customize offerings that are suited to the individual customer’s needs, in order to deliver the most effective security solutions. Everywhere from small stores to airports, our 310,000 employees are making a difference.
						<br><br>
						To explain our approach to responsible business, we go back to the very basics of security services: they provide tangible social and economic benefits – not only for our customers, but for a larger group of people and even whole communities.
						<br><br>
						By providing security solutions for customers in nearly all industries and segments and operating in a responsible and sustainable manner, our customers can focus on their core business and prosper.
						</p>
						<h4>Accessible and affordable services</h4>
						<p>Improving our services by making them accessible and affordable is one of our main responsibilities. Our services help us make a positive contribution to society and offer the greatest benefits to our stakeholders.</p>
						<h4>Responsible business is good business</h4>
						<p>
							Securitas’ sustainability work is based on our fundamental values – Integrity, Vigilance and Helpfulness – and guided by our key corporate policies and principles, such as Securitas’ Values and Ethics Code. To us, corporate social responsibility (CSR) is a business approach that delivers economic, social and environmental benefits to our stakeholders and adds value to the Securitas brand.
							<br><br>
							<b>Summarizing, conducting our business responsibly has a range of positive effects. We believe that responsible business is good business:</b>
							<br><br>
							- The global security services market employs several million people and is projected to reach USD 110 billion by 2016;<br>
							- Security services are in demand all over the world, in all industries and in both the public and private sectors;<br>
							- Demand for our services is closely linked to global economic development and social and demographic trends;<br>
							- As the global economy grows and develops, so do we.
							<br><br>
							Historically, the security market has grown 1–2 percent faster than GDP in mature markets. In recent years, due to current market dynamics and the gradual incorporation of technology into security solutions, security markets in Europe and North America have grown at the same pace as GDP. This trend is likely to continue over the next three to five years.
							<br><br>
							Market growth is crucial to Securitas’ future profitability and growth, but capitalizing on trends and changes in demand is also important. Developing new security solutions with a higher technology content and improved cost efficiency will allow the private security industry to expand the market by assuming responsibility for work presently performed by the police or other authorities. This development will also be a challenge for operations with insourced security services and increase interest in better outsourced solutions.
							<br><br>
							The global security services market employs several million people and is projected to reach USD 110 billion by 2016. Security services are in demand all over the world, in all industries and in both the public and private sectors. Demand for our services is closely linked to global economic development and social and demographic trends. As the global economy grows and develops, so do we.
							<br><br>
							Historically, the security market has grown 1–2 percent faster than GDP in mature markets. In recent years, due to current market dynamics and the gradual incorporation of technology into security solutions, security markets in Europe and North America have grown at the same pace as GDP. This trend is likely to continue over the next three to five years.
						</p>
					</div>
					
				</div>
			</div>
							
				<div class="main-sm-box">
		<h3 class="title">Sections</h3>
			<button class="accordion">Securitas Group</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>

<button class="accordion">Our Values</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>

<button class="accordion">Our Approach</button>
<div class="panel">
  <a href="#" class="line">Security Services North America</a>
  <a href="#" class="line">Security Services Europe</a>
  <a href="#" class="line">Security Services Ibero-America</a>
</div>
<button class="accordion">Our Partnerships</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>

<button class="accordion">Our Organization</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Our History</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Safety Act</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Our Responsibility</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Supplier Diversity Program</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Contact Us</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
</div>

		</div>
</div>
	<?php get_footer(); ?>