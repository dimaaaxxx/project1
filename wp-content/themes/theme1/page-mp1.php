<?php get_header(); ?>
<title>Media</title>
<div id="titler" class="wrap-f">
		<div class="wrap-s clr">
			<h2>Media</h2>

			<div class="breadcrumbs-box">
				<a href="http://wp.dev/" class="breadcrumbs-line"><span>Home</span></a>
				<a href="http://wp.dev/media/" class="breadcrumbs-line">Media</a>
				<a href="#" class="breadcrumbs-line"><?php the_title(); ?></a>
			</div>
		</div>
	</div>

	<div id="main" class="wrap-f">
		<div class="wrap-s clr">
			<div class="main-full-box">
				<div class="page-box clr">
					<h3 class="title"><?php the_title(); ?></h3>
					<div class="text-box">

								
						<?php the_content();?>
						
				
					</div>
								<div class="knews-box servises-box clr">
					<?php echo do_shortcode('[nggallery id=1]'); ?>
</div>
					
				</div>
			</div>
			
		</div>
	</div>
	<?php get_footer(); ?>