<?php get_header(); ?>

<div id="titler" class="wrap-f">
		<div class="wrap-s clr">
			<h2>News</h2>

			<div class="breadcrumbs-box">
				<a href="http://wordpress/" class="breadcrumbs-line"><span>Home</span></a>
				<a href="#" class="breadcrumbs-line">News</a>
			</div>
		</div>
	</div>

	<div id="main" class="wrap-f">
		<div class="wrap-s clr">
		
			

			<div class="main-lg-box">
				<div class="page-box clr">
					<h3 class="title">News</h3>
					<div class="text-box">
						<img src="<?php bloginfo('template_directory'); ?>/files/slide_1.jpg" alt="" class="full-image">
						<h4>Commercial Building Security</h4>
						<p>
							Securitas is a global knowledge leader in security. From a broad range of services of specialized guarding, technology solutions and consulting and investigations, we customize offerings that are suited to the individual customer’s needs, in order to deliver the most effective security solutions. Everywhere from small stores to airports, our 310,000 employees are making a difference.
							<br><br>
							The glue that holds together Securitas’ decentralized organization is a strong corporate culture. It is a culture distinguished by responsibility, ownership, entrepreneurship and what we call the Securitas Model.
						</p>
					</div> 

<?php if ( have_posts() ):  while ( have_posts() ) :the_post();?>
  <div class="last-news-box clr">

						<div class="last-news-line clr">
					
							
                                <a><?php the_post_thumbnail(); ?></a>
							
						     <div class="date"><?php the_time('d / m / y');?></div>
                 <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                 <?php the_content ("") ?>
                 					<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" class="read-more pseudo-link" >Read more..</a>
						
						</div>
	</div>                      


             
     <?php the_tags(); ?>
                       
                 
    
 <?php endwhile; ?>

<?php else :?>

<?php endif; ?>


<div class="clr"></div>
<div class="pagination clr">
<ul>
    <?php
        global $wp_query;
        $big = 999999999; 
               $args = 
        array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?page=%#%',
            'total' => $wp_query->max_num_pages,
            'current' => max( 1, get_query_var( 'paged') ),
            'show_all' => false,
            'end_size' => 3,
            'mid_size' => 2,
            'prev_next' => True,
            'prev_text' => __('< '),
            'next_text' => __('>'),
            'type' => 'list',
            );
        echo paginate_links($args);
    ?>
    </ul>
    </div>
    </div>

				</div>
				
				<div class="main-sm-box">
		<h3 class="title">Sections</h3>
			<button class="accordion">Securitas Group</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>

<button class="accordion">Our Values</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>

<button class="accordion">Our Approach</button>
<div class="panel">
  <a href="#" class="line">Security Services North America</a>
  <a href="#" class="line">Security Services Europe</a>
  <a href="#" class="line">Security Services Ibero-America</a>
</div>
<button class="accordion">Our Partnerships</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>

<button class="accordion">Our Organization</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Our History</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Safety Act</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Our Responsibility</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Supplier Diversity Program</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
<button class="accordion">Contact Us</button>
<div class="panel">
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
  <a href="#" class="line">Lorem ipsum dolor sit amet </a>
</div>
</div>

		</div>
</div>

<?php get_footer(); ?>