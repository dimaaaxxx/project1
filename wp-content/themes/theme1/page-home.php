<?php get_header(); ?>
<div class="owl-carousel">
		<div id="slider" class="wrap-f">
			<img class="slider-image" src="<?php bloginfo('template_directory'); ?>/files/slide_1_long.png">
			<div class="wrap-s">
				<div class="dot-box">
					<div class="dot-box-wrap">
						<a  class="dot-line active"></a>
						<a  class="dot-line"></a>
					</div>
				</div>
				<div class="info-box">
					<h3>SLIDE1</h3>
					<h2>TEST TEXT</h2>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>
				</div>
			</div>
			
		</div>
	<div id="slider" class="wrap-f">

		<img class="slider-image" src="<?php bloginfo('template_directory'); ?>/files/slide_1_long.png">
		<div class="wrap-s">
			<div class="dot-box">
				<div class="dot-box-wrap">
					<a  class="dot-line"></a>
					<a  class="dot-line active"></a>
				</div>
			</div>
			<div class="info-box">
				<h3>World Class</h3>
				<h2>SECURITY SERVICES</h2>
				<p>
					Securitas is a global knowledge leader in security. From a broad range of services of specialized guarding, technology solutions and consulting and investigations, we customize offerings that are suited to the individual customer’s needs.
				</p>
			</div>
		</div>
		
	</div>
</div>
<div id="slider1">
	<div class="business-box">
		<div class="wrap-s clr">
			<div class="business-form">
				<div class="search-form">
					<form action="http://wp.dev/contact/" method="POST">
						<input type="text" value="" name="company" placeholder="What business are you in?">
						<button type="submit" class="btn-blue"><i class="kicon search-white"></i></button>
					</form>
				</div>
			</div>
			<div class="business-message">Collective Protective Services provides world-class security services to businesses and organizations.</div>
		</div>
	</div>
	<a  class="arrow prev"><i class="kicon kicon-big2 arr-prev"></i></a>
	<a  class="arrow next"><i class="kicon kicon-big2 arr-next"></i></a>
</div>


 <div id="main" class="wrap-f clr">
		<div class="wrap-s clr">

			<div class="grid-info-box clr">
				<div class="kgrid4">
					<div class="grid-info-line">
						<h4 class="title">About Us</h4>
						<a href="http://wp.dev/about-us/" class="hoverlay-link"><div class="hoverlay" href="#"><span>MORE DETAILS</span></div>
						<img src="<?php bloginfo('template_directory'); ?>/files/home_photo_1.jpg">
</a>
						<p>Collective Protective Services provides armed and unarmed security guards to California businesses and organizations.</p>
					</div>
				</div>
				<div class="kgrid4">
					<div class="grid-info-line">
						<h4 class="title">Services</h4>
						<a href="http://wp.dev/services/" class="hoverlay-link"><div class="hoverlay" href="#"><span>MORE DETAILS</span></div>
						<img src="<?php bloginfo('template_directory'); ?>/files/home_photo_2.jpg"></a>
						<p>Collective Protective Services provides armed and unarmed security guards to California businesses and organizations.</p>
					</div>
				</div>
				<div class="kgrid4">
					<div class="grid-info-line">
						<h4 class="title">Training</h4>
						<a href="http://wp.dev/training/" class="hoverlay-link"><div class="hoverlay" href="#"><span>MORE DETAILS</span></div>
						<img src="<?php bloginfo('template_directory'); ?>/files/home_photo_3.jpg"></a>
						<p>Collective Protective Services provides armed and unarmed security guards to California businesses and organizations.</p>
					</div>
				</div>

			</div>

			<div class="clr h60"></div>

		</div>
	</div>
<?php get_footer(); ?>