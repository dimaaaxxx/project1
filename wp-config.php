<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress1');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pEq5KRPOGl[f;Kv_3Ux4|n<7{Fwfi/9,MFQA6mf Zb*{enJa^wKCc1#DP.@c7-<?');
define('SECURE_AUTH_KEY',  'y>l2pDQQfIpcN`7#czm?hvVWBOX6PxMp9A5!$[8cc7s(?0B$w9O7UTZh40gQ^CH8');
define('LOGGED_IN_KEY',    'x[yw.+Eqj,/}4g@88YXj0.Szc*/Kdh&UkYz_{[-;&%S%NelKC~7&|,@Aw}~]9Cb]');
define('NONCE_KEY',        'v>W9ISkUrEe 2)i7<t?NdZ#bF$udCy+:!4^Kg+~-T(TgSn&#/X300e|3@st,DC#]');
define('AUTH_SALT',        ')6&-pr))q#)5U<L0MsBf9veK8`Tj(r|_[<1EO0F?Hzk(Qd+YR!&S+!mb(w0:6t27');
define('SECURE_AUTH_SALT', ',Al9I_JENXYz)h:1mf)lpi;)ybpv+@&%bD EA5?59)yW;^l=Akr-Jf!~r/0J7T*!');
define('LOGGED_IN_SALT',   '=kiEZgh|@WoS];$grU>9Wl5e<iU/mf5p;~+6o*m:KD;E2IZ)l})nh*LoI92AO[+l');
define('NONCE_SALT',       ' ZNgv;Z(dgu<TyoKp*i[5[4Mh[%{+tkja!CY>;7T@P>^Z =s t:!G#mV>(3x>_$D');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_HOME', 'http://wp.dev/' );
define( 'WP_SITEURL', 'http://wp.dev/' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
