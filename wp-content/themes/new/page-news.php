<?php get_header(); ?>
<div id="titler" class="wrap-f">
		<div class="wrap-s clr">
			<h2>About Us</h2>

			<div class="breadcrumbs-box">
				<a href="#" class="breadcrumbs-line"><span>Home</span></a>
				<a href="#" class="breadcrumbs-line">About Us</a>
			</div>
		</div>
	</div>

	<div id="main" class="wrap-f">
		<div class="wrap-s clr">
			<div class="main-lg-box">
				<div class="page-box clr">
					<h3 class="title">News</h3>
					<div class="text-box">
						<img src="files/slide_1.jpg" alt="" class="full-image">
						<h4>Commercial Building Security</h4>
						<p>
							Securitas is a global knowledge leader in security. From a broad range of services of specialized guarding, technology solutions and consulting and investigations, we customize offerings that are suited to the individual customer’s needs, in order to deliver the most effective security solutions. Everywhere from small stores to airports, our 310,000 employees are making a difference.
							<br><br>
							The glue that holds together Securitas’ decentralized organization is a strong corporate culture. It is a culture distinguished by responsibility, ownership, entrepreneurship and what we call the Securitas Model.
						</p>
					</div>
					<div class="last-news-box clr">

						<div class="last-news-line clr">
							<a href="#"><img src="files/home_photo_4.jpg" alt=""></a>
							<div class="date">03 / 17 / 2014</div>
							<h4><a href="#">A High Level of Service</a></h4>
							<p>
								Security is Securitas' core business and we can provide our customers with a range of specialized services or a complete security solution. Here you will find information on our most frequent services.
							</p>
							<a href="#" class="read-more pseudo-link">Read more..</a>
						</div>

						<div class="last-news-line clr">
							<a href="#"><img src="files/home_photo_4.jpg" alt=""></a>
							<div class="date">03 / 24 / 2014</div>
							<h4><a href="#">Contributing to Public safety</a></h4>
							<p>
								A specially trained security officer performs services tailored to the needs of medium-sized and large businesses.
							</p>
							<a href="#" class="read-more pseudo-link">Read more..</a>
						</div>

						<div class="last-news-line clr">
							<a href="#"><img src="files/home_photo_4.jpg" alt=""></a>
							<div class="date">03 / 17 / 2014</div>
							<h4><a href="#">Helping Safeguard People at Work and Play</a></h4>
							<p>
								To meet the global growth expectations of our clients, Securitas offers security solutions that fit the specific needs of international organizations.
							</p>
							<a href="#" class="read-more pseudo-link">Read more..</a>
						</div>

						<div class="last-news-line clr">
							<a href="#"><img src="files/home_photo_4.jpg" alt=""></a>
							<div class="date">03 / 17 / 2014</div>
							<h4><a href="#">A High Level of Service</a></h4>
							<p>
								Security is Securitas' core business and we can provide our customers with a range of specialized services or a complete security solution. Here you will find information on our most frequent services.
							</p>
							<a href="#" class="read-more pseudo-link">Read more..</a>
						</div>

					</div>

					<div class="clr"></div>
					
					<div class="pagination clr">
						<a href="#">1</a>
						<a href="#" class="active">2</a>
						<a href="#">3</a>
						<span>..</span>
						<a href="#">12</a>

						<a href="#" class="prev"><i class="kicon arr-prev"></i></a>
						<a href="#" class="next"><i class="kicon arr-next"></i></a>
					</div>


				</div>
			</div>
			<div class="main-sm-box">
				<h3 class="title">Sections</h3>
				<div class="rubricator-box">
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Securitas Group</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Our Values</a>
					<a href="#" class="rubricator-line open"><i class="kicon kicon-big arr-right"></i>Our Approach</a>
					<div class="sub-rubricator-box" style="display:block;">
						<a href="#" class="sub-rubricator-line">Security Services North America</a>
						<a href="#" class="sub-rubricator-line active">Security Services Europe</a>
						<a href="#" class="sub-rubricator-line">Security Services Ibero-America</a>
					</div>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Our Partnerships</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Our Organization</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Our History</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Safety Act</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Our Responsibility</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Supplier Diversity Program</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Contact Us</a>
				</div>
			</div>
		</div>
	</div>
	<?php get_footer(); ?>