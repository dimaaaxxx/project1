<?php get_header(); ?>
	<div id="titler" class="wrap-f">
		<div class="wrap-s clr">
			<h2>About Us</h2>

			<div class="breadcrumbs-box">
				<a href="#" class="breadcrumbs-line"><span>Home</span></a>
				<a href="#" class="breadcrumbs-line">About Us</a>
			</div>
		</div>
	</div>

	<div id="main" class="wrap-f">
		<div class="wrap-s clr">
			<div class="main-lg-box">
				<div class="page-box">
					<h3 class="title">Contact Us</h3>
					<div class="text-box">
						<p>
							Securitas is a global knowledge leader in security. From a broad range of services of specialized guarding, technology solutions and consulting and investigations, we customize offerings that are suited to the individual customer’s needs, in order to deliver the most effective security solutions. Everywhere from small stores to airports, our 310,000 employees are making a difference.
						</p>
						<div class="kgrid">
							<div class="kcol6">
								<h5>Address:</h5>
								<p>
									995 New Ave, Office 7<br>
									Wonderland, CA 94107, USA
								</p>
							</div>
							<div class="kcol6">
								<h5>E-mail:</h5>
								<p>
									info@cpssecurity.com<br>
									support@cpssecurity.com
								</p>
							</div>
							<div class="clr"></div>
							<div class="kcol6">
								<h5>Phone &amp; Fax:</h5>
								<p>
									<span class="blue">+440</span> 353 363 15 16<br>
									<span class="blue">+440</span> 353 363 15 17
								</p> 
							</div>
							<div class="kcol6">
								<h5>Social Network:</h5>
								<div class="social-box">
									<a href="#" class="social-line"><i class="kicon soc-fb-b"></i></a>
									<a href="#" class="social-line"><i class="kicon soc-tw-b"></i></a>
									<a href="#" class="social-line"><i class="kicon soc-gp-b"></i></a>
									<a href="#" class="social-line"><i class="kicon soc-in-b"></i></a>
									<a href="#" class="social-line"><i class="kicon soc-vm-b"></i></a>
					  			</div>
					  			<p></p>
							</div>
							<div class="clr"></div>
						</div>
						<p>
							The glue that holds together Securitas’ decentralized organization is a strong corporate culture. It is a culture distinguished by responsibility, ownership, entrepreneurship and what we call the Securitas Model.
						</p>

						<h3 class="title">Contact Form</h3>
						<div class="form-box kgrid">
							<form>
								<div class="kcol6"><input type="text" placeholder="Your Name..."></div>
								<div class="kcol6"><input type="text" placeholder="Your Company..."></div>
								<div class="kcol6"><input type="text" placeholder="Your Email Address..."></div>
								<div class="kcol6"><input type="text" placeholder="Your Phone..."></div>
								<textarea placeholder="Your Message..."></textarea>
								<div class="submiter"><button type="submit" class="btn-blue">Send Message</button></div>
							</form>
						</div>

					</div>
					
				</div>
			</div>
			<div class="main-sm-box">
				<h3 class="title">Sections</h3>
				<div class="rubricator-box">
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Securitas Group</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Our Values</a>
					<a href="#" class="rubricator-line open"><i class="kicon kicon-big arr-right"></i>Our Approach</a>
					<div class="sub-rubricator-box" style="display:block;">
						<a href="#" class="sub-rubricator-line">Security Services North America</a>
						<a href="#" class="sub-rubricator-line active">Security Services Europe</a>
						<a href="#" class="sub-rubricator-line">Security Services Ibero-America</a>
					</div>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Our Partnerships</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Our Organization</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Our History</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Safety Act</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Our Responsibility</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Supplier Diversity Program</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Contact Us</a>
				</div>
			</div>
		</div>
	</div>

	<?php get_footer(); ?>