<?php get_header(); ?>
<body>

	<div id="titler" class="wrap-f">
		<div class="wrap-s clr">
			<h2>About Us</h2>

			<div class="breadcrumbs-box">
				<a href="#" class="breadcrumbs-line"><span>Home</span></a>
				<a href="#" class="breadcrumbs-line">About Us</a>
			</div>
		</div>
	</div>

	<div id="main" class="wrap-f">
		<div class="wrap-s clr">
			<div class="main-lg-box">
				<div class="page-box no-overflow">
					<h3 class="title">About Us</h3>
					<div class="text-box">
						<img src="<?php bloginfo('template_directory'); ?>/files/slide_1.jpg" alt="" class="full-image">
						<p>
							Securitas is a global knowledge leader in security. From a broad range of services of specialized guarding, technology solutions and consulting and investigations, we customize offerings that are suited to the individual customer’s needs, in order to deliver the most effective security solutions. Everywhere from small stores to airports, our 310,000 employees are making a difference.
							<br><br>
							The glue that holds together Securitas’ decentralized organization is a strong corporate culture. It is a culture distinguished by responsibility, ownership, entrepreneurship and what we call the Securitas Model.
						</p>

						<div class="knews-box clr">
							<div class="knews-line">
								<a href="#" class="hlink">
									<img src="<?php bloginfo('template_directory'); ?>/files/home_photo_2.jpg">
									<h3>Asis International 60th Annual Expo</h3>
								</a>
								<p>
									We provide a wide array of services to our clients to ensure that all security needs are met. 
									From standing officers to site patrol, we maintain the security of your business.
								</p>
								<a href="#" class="read-more pseudo-link">Read more..</a>
							</div>
							<div class="knews-line">
								<a href="#" class="hlink">
										<img src="<?php bloginfo('template_directory'); ?>/files/home_photo_3.jpg">
									<h3>Asis International 60th Annual Expo</h3>
								</a>
								<p>
									We offer all BSIS & NRA certificates to military as well as civilians through our state of the art in-house security training facilities.
								</p>
								<a href="#" class="read-more pseudo-link">Read more..</a>
							</div>
							<div class="clr"></div>
							<div class="knews-line">
								<a href="#" class="hlink">
										<img src="<?php bloginfo('template_directory'); ?>/files/home_photo_2.jpg">
									<h3>Asis International 60th Annual Expo</h3>
								</a>
								<p>
									We provide a wide array of services to our clients to ensure that all security needs are met. 
									From standing officers to site patrol, we maintain the security of your business.
								</p>
								<a href="#" class="read-more pseudo-link">Read more..</a>
							</div>
							<div class="knews-line">
								<a href="#" class="hlink">
										<img src="<?php bloginfo('template_directory'); ?>/files/home_photo_2.jpg">
									<h3>Asis International 60th Annual Expo</h3>
								</a>
								<p>
									We provide a wide array of services to our clients to ensure that all security needs are met. 
									From standing officers to site patrol, we maintain the security of your business.
								</p>
								<a href="#" class="read-more pseudo-link">Read more..</a>
							</div>

						</div>

					</div>
					
				</div>
			</div>
			<div class="main-sm-box">
				<h3 class="title">Sections</h3>
				<div class="rubricator-box">
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Securitas Group</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Our Values</a>
					<a href="#" class="rubricator-line open"><i class="kicon kicon-big arr-right"></i>Our Approach</a>
					<div class="sub-rubricator-box" style="display:block;">
						<a href="#" class="sub-rubricator-line">Security Services North America</a>
						<a href="#" class="sub-rubricator-line active">Security Services Europe</a>
						<a href="#" class="sub-rubricator-line">Security Services Ibero-America</a>
					</div>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Our Partnerships</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Our Organization</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Our History</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Safety Act</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Our Responsibility</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Supplier Diversity Program</a>
					<a href="#" class="rubricator-line"><i class="kicon kicon-big arr-right"></i>Contact Us</a>
				</div>
			</div>
		</div>
	</div>

	<?php get_footer(); ?>