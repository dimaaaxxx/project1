<?php get_header(); ?>
<title>Media</title>
<div id="titler" class="wrap-f">
		<div class="wrap-s clr">
			<h2>Media</h2>

			<div class="breadcrumbs-box">
				<a href="http://wp.dev/" class="breadcrumbs-line"><span>Home</span></a>
				<a href="#" class="breadcrumbs-line">Media</a>
			</div>
		</div>
	</div>

	<div id="main" class="wrap-f">
		<div class="wrap-s clr">
			<div class="main-full-box">
				<div class="page-box clr">
					<h3 class="title">Media</h3>
					<div class="text-box">

						<p>			
						Securitas is a global knowledge leader in security. From a broad range of services of specialized guarding, technology solutions and consulting and investigations, we customize offerings that are suited to the individual customer’s needs, in order to deliver the most effective security solutions. Everywhere from small stores to airports, our 310,000 employees are making a difference.</p>
						
				
					</div>
								<div class="knews-box servises-box clr">
					<?php
					$mypages = get_pages( array( 'child_of' => $post->ID, 'sort_column' => 'post_date', 'sort_order' => 'desc' ) );

					foreach( $mypages as $page ) {
						$content = $page->post_content;
						$content = apply_filters( 'the_content', $content );
						$thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id($page->ID), 'full' ); 
						?>
						<div class="knews-line">
							 <a href="<?php echo get_page_link( $page->ID ); ?>"class="hlink">
						<img src="<?php echo $thumbnail_attributes[0] ?>" alt="" class="full-image">
								<h3><?php echo $page->post_title; ?></h3>
							</a>
							<?php echo $content; ?>
							<a href="<?php echo get_page_link( $page->ID ); ?>" class="read-more pseudo-link">Read more..</a>
						</div>
						<?php
					}?>

</div>
					
				</div>
			</div>
			
		</div>
	</div>
	<?php get_footer(); ?>