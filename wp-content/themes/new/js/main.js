
jQuery(document).ready(function(){
  jQuery('.owl-carousel').owlCarousel({
    loop: false,
    items: 1,
    dots: true,
    dotsClass: '.dot-box'
});
owl = jQuery('.owl-carousel').owlCarousel();
jQuery(".arrow.prev").click(function () {
    owl.trigger('prev.owl.carousel');
});

jQuery(".arrow.next").click(function () {
    owl.trigger('next.owl.carousel');
});
jQuery('.dot-line').click(function () {
    owl.trigger('to.owl.carousel', [jQuery(this).index(), 300]);
});
});
