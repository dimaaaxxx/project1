
jQuery(function(){
	//togggle search form
	jQuery('body')
		.on('click', '#header .search-control', function(e){
			e.preventDefault();
			jQuery(this).closest('.search-box').toggleClass('open');
			e.stopPropagation();
		})
		.on('click', '#header .search-form', function(e){
			e.stopPropagation();
		})
		.on('click', function(e){
			if(jQuery('#header .search-box').hasClass('open')){
				jQuery('#header .search-box').removeClass('open');
			}
		});


	jQuery('.rubricator-box').find('.rubricator-line').removeClass('open');

	//rubricator
	jQuery('.rubricator-box .rubricator-line')
		.on('click', function(e) {
			if(jQuery(this).next().hasClass('sub-rubricator-box')){
				e.preventDefault();
				jQuery(this).closest('.rubricator-box').find('.rubricator-line').removeClass('open');
				jQuery(this).closest('.rubricator-box').find('.sub-rubricator-box').hide();
				
				jQuery(this).addClass('open');
			jQuery(this).next().show();
			}
		});
});

	jQuery(document).ready(function(){
  jQuery('.owl-carousel').owlCarousel({
    loop: false,
    items: 1,
    dots: true,
    dotsClass: '.dot-box'
});
owl = jQuery('.owl-carousel').owlCarousel();
jQuery(".arrow.prev").click(function () {
    owl.trigger('prev.owl.carousel');
});

jQuery(".arrow.next").click(function () {
    owl.trigger('next.owl.carousel');
});
jQuery('.dot-line').click(function () {
    owl.trigger('to.owl.carousel', [jQuery(this).index(), 300]);
});
});
jQuery(document).ready(function(){
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + 'px';
    } 
  }
}
});